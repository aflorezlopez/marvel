import Vue from 'vue/dist/vue';
import Vuex from 'vuex'
import modal from './modules/modalStore'
import comic from './modules/comicStore'
import favorite from './modules/favoriteStore'
import pager from './modules/pagerStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    modal,
    comic,
    favorite,
    pager,
  }
})