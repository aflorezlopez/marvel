// initial state
const state = {
  pages: [0,1,2,3,4],
  activePage: 0,
  offset: 0,
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
  updatePagerStore (state, newPager) {
    state.pages = newPager;
  },
  updateActivePage (state, page) {
    state.activePage = page;
  },
  updateOffset (state, offset) {
    state.offset = offset;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}