// initial state
const state = {
  characters: {}
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
  updateCharacters (state, characters) {
    state.characters = characters;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}