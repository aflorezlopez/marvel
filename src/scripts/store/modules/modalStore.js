// initial state
const state = {
  comic: {
    classHide: 'hide',
    id: '',
    img: '',
    title: '',
    description: '',
  },
  character: {
    classHide: 'hide',
    id: '',
    img: '',
    title: '',
    description: '',
  },
  empty: true,
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
  changeComic (state, newComic) {
    state.comic = {
      id: newComic.id,
      img: newComic.thumbnail.path+'.'+newComic.thumbnail.extension,
      title: newComic.title,
      classHide: 'show',
      description: newComic.description,
    }
    state.empty = false;
  },
  closeModal (state, classHide) {
    state.comic.classHide = 'hide';
  },
  changeCharacter (state, newCharacter) {
    
    if (newCharacter.description === "") {
      newCharacter.description = "No Description";
    }

    state.character = {
      id: newCharacter.id,
      img: newCharacter.thumbnail.path+'.'+newCharacter.thumbnail.extension,
      title: newCharacter.name,
      classHide: 'show',
      description: newCharacter.description,
    }
  },
  closeModalCharacter (state) {
    state.character.classHide = 'hide';
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}