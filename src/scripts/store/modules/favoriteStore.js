// initial state
const state = {
  favorites: []
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
  updateFavorites (state, favorite) {
    state.favorites.push(favorite);
  },
  deleteFavorite (state, favoriteId) {
    var favorites = state.favorites;
    for(var i in favorites) {
      if (favoriteId === favorites[i].id) {
        favorites.splice(i, 1);
      }
    }
    state.favorites = favorites;
  },
  randomComic(state, comic) {
    state.favorites.push(favorite);
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}