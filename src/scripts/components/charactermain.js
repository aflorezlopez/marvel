export default {
	data() {
		return{
		};
	},

	props: ['character', 'comicmain'],

	template: "#charactermain-template",

	mounted() {
	},

	methods: {
		viewComic: function(resourceURI, $event) {
		
			$event.preventDefault();
			var partUrl = resourceURI.split("/");
			var idComic = partUrl[partUrl.length-1];
			var params = window.params;

			this.$http.jsonp('comics/'+idComic, {params: params} )
	      .then(response => {
	      	
			    var comic = response.body.data.results["0"];
			    
			    if (comic.description === null) {
			    	comic.description = 'No description';
			    }

			    this.$store.commit('changeComic', comic);
			  
			  }, response => {
			});
		},
		viewMore: function(id, $event) {
			$event.preventDefault();
			var params = window.params;

			this.$http.jsonp('characters/'+id, {params: params} )
	      .then(response => {
			    var character = response.body.data.results["0"];
			    this.$store.commit('changeCharacter', character);
			  
			  }, response => {
			});
		}
	}
}