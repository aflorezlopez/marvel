import charactermodal from './charactermodal';

export default {
	data() {
		return{
			favorites: {},
			detailComic: {},
			urlComic: '',
			params: window.params,
			characterName: ''
		};
	},

	components: { charactermodal },

	template: "#header-template",

	mounted() {
	},

	methods: {
		searchCharacter: function($event){
			var params = this.params;

			if (this.characterName === '') {
				delete params.nameStartsWith;
			}else if(this.characterName !== '') {
				params.nameStartsWith = this.characterName;
			}

			this.$http.jsonp('characters', {params: params} )
	      .then(response => {
	      	var comic = response.body.data.results;
	      	this.$store.commit('updateCharacters', comic);
			  }, response => {
			});
		}
	}
}