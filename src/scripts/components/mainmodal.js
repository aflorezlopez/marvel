export default {
	
	data() {
		return{
			params: window.params,
		};
	},
	
	template: "#charactermodalmain-template",

	computed: {
    changeCharacter: function () {
      return this.$store.state.modal.character;
    },
    classHide: function () {
    	return this.$store.state.modal.character.classHide;
    }
  },

  mounted() {
	},

	methods: {
		closeModal: function() {
			this.$store.commit('closeModalCharacter');
		}
	}
}