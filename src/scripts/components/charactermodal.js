export default {
	
	data() {
		return{
			// classHide: 'hide',
			params: window.params,
		};
	},

	props: ['comic'],

	template: "#charactermodal-template",

	computed: {
    changeComic: function () {
      return this.$store.state.modal.comic;
    },
    classHide: function () {
    	return this.$store.state.modal.comic.classHide;
    }
  },
	
	mounted() {

	},

	methods: {
		addFavorite: function(idComic) {
			var params = this.params;

			this.$http.jsonp('comics/'+idComic, {params: params} )
	      .then(response => {
	      	
			    var comic = response.body.data.results["0"];
			    this.$store.commit('updateFavorites', comic);
			  
			  }, response => {
			});
		},
		closeModal: function() {
			this.$store.commit('closeModal');
		}
	}
}