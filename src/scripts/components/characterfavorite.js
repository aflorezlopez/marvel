export default {
	data() {
		return{
		};
	},

	props: ['favorite'],

	template: "#characterfavorite-template",

	mounted() {
	},

	methods: {
		removeFavorite: function(idFavorite) {
			
			var favorites = localStorage.getItem('favorites');
			console.log(idFavorite);
			this.$store.commit('deleteFavorite', idFavorite);

			favorites = JSON.stringify(this.$store.state.favorite.favorites);
			localStorage.setItem( 'favorites', favorites );
		}
	}
}