export default {
	data() {
		return{
		};
	},

	template: "#contentfavorite-template",

	mounted() {
		var favStorage = localStorage.getItem('favorites');
		var favorites = JSON.parse(favStorage);

		for (var i in favorites){
			this.$store.commit('updateFavorites', favorites[i]);
		}
	},

	computed: {
    favorites: function () {

    	var favorites = localStorage.getItem('favorites');

    	if (this.$store.state.favorite.favorites.length > 0) {
    		favorites = JSON.stringify(this.$store.state.favorite.favorites);
    	}

    	if (favorites !== null) {
    		localStorage.setItem( 'favorites', favorites );
    	}
    	
      return this.$store.state.favorite.favorites;
    }
  },

	methods: {
		getRandomInt: function(min, max) {
    	return Math.floor(Math.random() * (max - min + 1)) + min;
		}
	}
}