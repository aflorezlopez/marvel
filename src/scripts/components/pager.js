export default {
	data() {
		return{
			params: window.params
		};
	},

	template: "#pager-template",

	mounted() {
	},

	computed: {
    pages: function () {
      return this.$store.state.pager.pages;
    },
    activePage: function () {
    	return this.$store.state.pager.activePage;
    }
  },

	methods: {
		selectPage: function(offset, page) {
			var params = this.params;
			params.offset = offset;

			this.$http.jsonp('characters', {params: params} )
	      .then(response => {
	      	
	      	var comic = response.body.data.results;
	      	this.$store.commit('updateCharacters', comic);
	      	this.$store.commit('updateOffset', offset);

	      	this.updatePager(page);

			  }, response => {
			});

		},
		updatePager(page) {
			
			var pagerCurrent = this.pages;
			var currentPage = this.$store.state.pager.activePage;
			var increment = page - currentPage;

			for (var prop in pagerCurrent) {				
				pagerCurrent[prop] = pagerCurrent[prop] + increment;
			}

			this.$store.commit('updatePagerStore', pagerCurrent);
			this.$store.commit('updateActivePage', page);
		},
		previousPage: function() {
			var currentPage = this.$store.state.pager.activePage;
			var page = currentPage-1;
			var offset = this.$store.state.pager.offset-10;
			this.selectPage(offset, page);
		},
		nextPage: function() {
			var offset = this.$store.state.pager.offset+10;
			var currentPage = this.$store.state.pager.activePage;
			var page = currentPage+1;
			this.selectPage(offset, page);
		}

	}
}