import pager from './pager';

export default {
	data() {
		return{
			params: window.params,
			sortby: ''
		};
	},

	template: "#contentmain-template",

	components: {pager},

	mounted() {
		this.getCharacters();
	},

	computed: {
    characters: function () {
      return this.$store.state.comic.characters;
    }
  },

	methods: {

		getCharacters: function() {
			
			var params = this.params;

			this.$http.jsonp('characters', {params: params} )
	      .then(response => {
	      	var comic = response.body.data.results;
	      	this.$store.commit('updateCharacters', comic);
			  }, response => {
			});
		},
		randomComics: function(comic) {

		},
		orderBy: function() {
			
			var params = this.params;
			params.orderBy = this.sortby;

			this.$http.jsonp('characters', {params: params} )
	      .then(response => {
	      	var comic = response.body.data.results;
	      	this.$store.commit('updateCharacters', comic);
			  }, response => {
			});
		}
	}
}