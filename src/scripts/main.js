import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router';
import Header from './components/header';
import Content from './components/contentmain';
import CharacterMain from './components/charactermain';
import ContentFavorite from './components/contentfavorite';
import CharacterFavorite from './components/characterfavorite';
import Footer from './components/footer';
import Modal from './components/charactermodal';
import Pager from './components/pager';
import VueResource from 'vue-resource';
import Vuex from 'vuex'
import store from './store'
import mainmodal from './components/mainmodal';

Vue.component('navigation', Header);
Vue.component('contentmain', Content);
Vue.component('charactermain', CharacterMain);
Vue.component('contentfavorite', ContentFavorite);
Vue.component('characterfavorite', CharacterFavorite);
Vue.component('footergrability', Footer);
Vue.component('charactermodal', Modal);
Vue.component('mainmodal', mainmodal);
Vue.component('pager', Pager);

Vue.use(VueRouter);
Vue.use(VueResource);

const routes = [
	{path: "/", component: Header},
]

const router = new VueRouter({
	routes,
});

window.params = {
	'apikey': '35926598b0b0197103116c0dcd2fc492',
	'hash': '84bcd3ad0e0ff763ea3cd6bf96d56fa6',
	'ts': '1',
	'limit': 10,
}

Vue.http.options.root = "http://gateway.marvel.com/v1/public";

new Vue({
	store,
	router
}).$mount('#main')